package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {

    }

    public static void multiplesOfSevenUpTo(int n) {
        int x = 7;
        while (x < n+1){
            System.out.println(x);
            x += 7;
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++){
            String text = i + ": ";
            for (int j = 1; j <= n; j++){
                text += i*j + " ";
            }
            System.out.println(text);
        }
    }

    public static int crossSum(int num) {
        String digits_as_string = Integer.toString(num);
        if (digits_as_string.length() <= 1){
            System.out.println(num);
        }
        int n = digits_as_string.length();
        int sum = 0;
        for (int i = 0; i < n; i++){
            int digit_in_place = Character.getNumericValue(digits_as_string.charAt(i));
            sum += digit_in_place;
        }
        return sum;
    }
}