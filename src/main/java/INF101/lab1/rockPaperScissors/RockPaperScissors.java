package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        boolean continueGame = true;
        while (continueGame) {
            // Printar ut kva runde som startar
            System.out.println("Let's play round " + roundCounter);

            // Hentar inn valget til spelaren (input)
            String playerChoice = getPlayerChoice();

            // Hentar inn valget til datamaskina (tilfeldig)
            String computerChoice = getComputerChoice();

            // Reknar ut kven som vinn, og oppdaterer stillinga
            String winner = getWinnerAndUpdateScore(playerChoice, computerChoice);

            // Printar ut valga samt kven som vann
            System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". " + winner);

            // Printar ut stillinga
            System.out.println("Score: human " + humanScore  + ", " + "computer " + computerScore);

            // Sjekkar om spelar vil halda fram
            continueGame = checkIfUserWantsToContinue();
        }
        System.out.println("Bye bye :)");
        // TODO: Implement me :)
    }

    public boolean checkIfUserWantsToContinue() {
        while (true) {
            String playerChoice = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (playerChoice.equals("y")) {
                this.roundCounter += 1;
                return true;
            } else if (playerChoice.equals("n")) {
                return false;
            }
            System.out.println("I do not understand " + playerChoice + ". Could you try again?");
        }
    }

    private String getPlayerChoice() {
        while (true) {

            String playerChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            switch (playerChoice) {
                case "rock":

                    return "rock";

                case "paper":

                    return "paper";

                case "scissors":

                    return "scissors";

                default:
                    System.out.println("I do not understand " + playerChoice + ". Could you try again?");
            }
        }
    }

    private String getComputerChoice() {
        Random rand = new Random();
        int computerChoiceAsInt = (int) (rand.nextInt(3));
        return rpsChoices.get(computerChoiceAsInt);

    }

    private String getWinnerAndUpdateScore(String playerChoice, String computerChoice) {
        String computerWins = "Computer wins!";
        String humanWins = "Human wins!";
        if (playerChoice.equals(computerChoice)){
            return "It's a tie!";
        }
        if (playerChoice.equals("rock")){
            if(computerChoice.equals("paper")){
                this.computerScore += 1;
                return "Computer wins!";
            }
        }
        if (playerChoice.equals("paper")){
            if(computerChoice.equals("scissors")){
                this.computerScore += 1;
                return computerWins;
            }
        }
        if (playerChoice.equals("scissors") && computerChoice.equals("rock")){
            this.computerScore += 1;
            return computerWins;
        }
        this.humanScore += 1;
        return humanWins;

    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}