package INF101.lab1.INF100labs;

import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }
    
    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        for (int i = 0; i < grid.size() - 1; i++){
            int sumOfRow = 0;
            int sumOfNextRow = 0;
            for (int j = 0; j < grid.get(i).size(); j++){
                sumOfRow += grid.get(i).get(j);
                sumOfNextRow += grid.get(i+1).get(j);
            }
            // grid.get(i).forEach(elementInRow -> {
            //     sumOfRow = sumOfRow + elementInRow;
            // });
            // grid.get(i+1).forEach(elementInRow -> {
            //     sumOfNextRow += elementInRow;
            // });
            if(sumOfRow != sumOfNextRow){
                return false;
            }
        }
        for (int i = 0; i < grid.get(0).size() - 1; i++){
            int sumOfColumn = 0;
            int sumOfNextColumn = 0;
            for (int j = 0; j < grid.get(i).size(); j++){
                sumOfColumn += grid.get(j).get(i);
                sumOfNextColumn += grid.get(j).get(i+1);
            }
            // grid.forEach(row -> {
            //     sumOfColumn += row.get(j);
            //     sumOfNextColumn += row.get(j+1);
            // });
            if(sumOfColumn != sumOfNextColumn){
                return false;
            }
        }
        return true;
    }

}