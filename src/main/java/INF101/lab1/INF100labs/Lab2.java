package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
    }
    

    public static void findLongestWords(String word1, String word2, String word3) {
        if (word1.length() >= word2.length() && word1.length() >= word3.length()){
            System.out.println(word1);
        }
        if (word2.length() >= word1.length() && word2.length() >= word3.length()){
            System.out.println(word2);
        }
        if (word3.length() >= word1.length() && word3.length() >= word2.length()){
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0){
            return true;
        }
        else if (year % 100 == 0){
            return false;
        }
        else if (year % 4 == 0){
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0){
            return true;
        }
        return false;
    }

}
