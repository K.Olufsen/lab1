package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // System.out.println(multipliedWithTwo(new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4))));
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            int numberMultipliedByTwo = list.get(i) * 2;
            newList.add(numberMultipliedByTwo);
        }
        return newList;
        // ArrayList<Integer> mylist = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        // throw new UnsupportedOperationException("Not implemented yet.");
        //Testa commit, endå ein test
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        // for (int i = 0; i < list.size(); i++) {
        //     int currentValue = list.get(i);
        //     if(currentValue != 3) {
        //         newList.add(currentValue);
        //     }
        // }
        // for (int currentValue : list) {
        //     if (currentValue != 3) {
        //         newList.add(currentValue);
        //     }
        // }
        list.forEach(currentValue -> {
            if(currentValue != 3){
                newList.add(currentValue);
            }
        });
        return newList;
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        list.forEach(currentValue -> {
            if(!newList.contains(currentValue)){
                newList.add(currentValue);
            }
        });
        return newList;
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++){
            a.set(i, a.get(i) + b.get(i));
        };
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

}